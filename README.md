# Library example 

Library: Books, Students, and Issued Books


## Run application 

To run the application in local environment (Ubuntu/Linus) go through next steps
1. `git clone git@gitlab.com:djm.im/nlf.git`
2. `cd nlf`
3. `mysql -u [DBUSERNAME] -p -e "CREATE DATABASE [DBNAME];"`  
    Example: `mysql -u root -p -e "CREATE DATABASE library;"`  
    [Note 1](note-1)

4. `export DATABASE_URL="mysql://localhost:3306/[DBNAME]?user=[USERNAME]&password=[PASSWORD]"`  
    Replace `[USERNAME]` and `[PASSWORD]` with proper values
    
5. `mvn package`
6. `java -jar target/dependency/webapp-runner.jar target/*.war`
7.  Open http://localhost:8080/  
    [Note 2](note-2)

### Notes 
#### Note 1  
Database will be created automaticlly on start application.  
Another way to create database is from sql script (this script is in `nlf/sql/library.sql`) by command  
`mysql -u [DBUSERNAME] -p < sql/library.sql`  

#### Note 2
8080 is default port.  
To use another port is used parameter `--port`  
`java -jar target/dependency/webapp-runner.jar target/*.war --port 9090`  
The value has to be greater than 1024.


## Development 

### Eclipse 
1. Check out code from Gitlab
2. In Eclipse: `File` > `Import..`
3. Chose 'Existing Maven Project'
4. Add Tomcat server and add Library to the server
5. Open http://localhost:8080/Library

## Gitlab Build

To build project on Gitlab it is required to define `.gitlab-ci.yml` file.  
This file is in root of the project.

## Deploy to Heroku 

1. Create an application on Heroku - https://dashboard.heroku.com/new-app  

2. Update `.gitlab-ci.yml` file   
```
deploy:
  stage: deploy
  script:
    - apt-get update -yq
    - apt-get install rubygems ruby-dev -y
    - gem install dpl
    - dpl --provider=heroku --app=[APP_NAME] --api-key=$HEROKU_API_KEY
```
Replace [APP_NAME] with value from Heroku (the name of application on Heroku)

3. Add `$HEROKU_API_KEY` to Gitlab
    * Open https://dashboard.heroku.com/account and find 'API key' section 
    * Copy the app key
    * Open Gitlab 'Settings' --> 'CI / CD' https://gitlab.com/ccjobs/bitsmithjobs-rest-service/settings/ci_cd 
        * Expand Secret variabls 
        * Add Heroku API key

4. To run in Heroku it is needed to add `Procfile`  
    `web:    java $JAVA_OPTS -jar target/dependency/webapp-runner.jar --port $PORT target/*.war` 
    
    Also, it is needed to webapp-runner is added through maven 
    pom.xml
```
    <execution>
		<phase>package</phase>
		<goals>
			<goal>copy</goal>
		</goals>
		<id>webapp-runner-heroku</id>
		<configuration>
			<artifactItems>
				<artifactItem>
					<groupId>com.github.jsimone</groupId>
					<artifactId>webapp-runner</artifactId>
					<version>8.5.23.0</version>
					<destFileName>webapp-runner.jar</destFileName>
				</artifactItem>
			</artifactItems>
		</configuration>
	</execution>
```

5. Add database - JawsDB MySQL
    * In tab 'Resource' type JawsDB and add this add-on
    * It will be automatically added variable `JAWSDB_URL`
        * This variable we read by Java 
            `String getenv = System.getenv("JAWSDB_URL");`
    * JawsDB in free plan has limit of 10 connonection to database
