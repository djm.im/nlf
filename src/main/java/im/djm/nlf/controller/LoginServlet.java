package im.djm.nlf.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import im.djm.nlf.data.Librarian;
import im.djm.nlf.exceptions.LibraryException;
import im.djm.nlf.service.LibrarianService;

public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = -6272233265934243098L;

	private static final String INDEX_JSP_PAGE = "index.jsp";

	private static final String STUDENT_PAGE = "student";

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nextPage = proceedRequestAndGetNextPage(request);

		request.getRequestDispatcher(nextPage).forward(request, response);
	}

	private String proceedRequestAndGetNextPage(HttpServletRequest request) {
		String action = request.getParameter("action");

		return proceedLibrarian(request, action);
	}

	private String proceedLibrarian(HttpServletRequest request, String action) {
		switch (action) {
		case "signup":
			return createNewLibrarianInDatabase(request);
		case "signin":
			return authenticateLibrarian(request);
		default:
			return INDEX_JSP_PAGE;
		}
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private String authenticateLibrarian(HttpServletRequest request) {
		boolean authenticationSuccessful = false;
		try {
			Librarian librarian = fetchLibrarianFromRequest(request);
			authenticationSuccessful = LibrarianService.getInstance().authenticateLibrarian(librarian);

			if (!authenticationSuccessful) {
				request.setAttribute("errors", new String[] { "Wrong username or password." });
			} else {
				request.getSession().setAttribute("librarian", librarian);
			}
		} catch (LibraryException ex) {
			request.setAttribute("errors", ex.getErrorMessages());
		}

		return (authenticationSuccessful) ? STUDENT_PAGE : INDEX_JSP_PAGE;
	}

	private String createNewLibrarianInDatabase(HttpServletRequest request) {
		try {
			Librarian newLibrarian = createNewLibrarianFromRequest(request);

			boolean librarianCreatedSuccessful = LibrarianService.getInstance().createLibratian(newLibrarian);
			if (librarianCreatedSuccessful) {
				request.setAttribute("message", "Successfully created new user.");
			}
		} catch (LibraryException ex) {
			request.setAttribute("errors", ex.getErrorMessages());
		}
		return INDEX_JSP_PAGE;
	}

	private Librarian fetchLibrarianFromRequest(HttpServletRequest request) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		return new Librarian(username, password);
	}

	private Librarian createNewLibrarianFromRequest(HttpServletRequest request) throws LibraryException {
		String username = request.getParameter("username");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		if (!password1.equals(password2)) {
			throw new LibraryException("Entered passwords are not the same.");
		}
		return new Librarian(username, password1);
	}

}
