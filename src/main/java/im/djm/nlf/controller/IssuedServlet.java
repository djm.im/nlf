package im.djm.nlf.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import im.djm.nlf.data.Book;
import im.djm.nlf.data.Issued;
import im.djm.nlf.data.Student;
import im.djm.nlf.exceptions.LibraryException;
import im.djm.nlf.service.BookService;
import im.djm.nlf.service.IssuedService;
import im.djm.nlf.service.StudentService;

public class IssuedServlet extends HttpServlet {

	private static final long serialVersionUID = 6053513380163971065L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		ActionType actionType = ActionType.getFor(action);

		try {
			switch (actionType) {
			case LIST: {
				break;
			}
			case ADD: {
				Issued issued = createIssue(request, actionType);
				IssuedService.getInstance().add(issued);
				break;
			}
			case DELETE: {
				Issued issued = createIssue(request, actionType);
				IssuedService.getInstance().delete(issued);
				break;
			}
			default:
				break;
			}
			if (actionType != ActionType.LIST) {
				request.setAttribute("message", "Action successfully executed.");
			}
		} catch (LibraryException ex) {
			request.setAttribute("errors", ex.getErrorMessages());
		}

		List<Issued> issues = IssuedService.getInstance().findAll();
		request.setAttribute("issues", issues);

		List<Student> students = StudentService.getInstance().findAll();
		request.setAttribute("students", students);

		List<Book> books = BookService.getInstance().findAll();
		request.setAttribute("books", books);

		request.getRequestDispatcher("/WEB-INF/pages/issued.jsp").forward(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private Issued createIssue(HttpServletRequest request, ActionType actionType) {
		Integer pkIssued = null;
		if (actionType == ActionType.EDIT || actionType == ActionType.DELETE) {
			String pkIssuedParam = request.getParameter("pkIssued");
			if (pkIssuedParam != null) {
				pkIssued = Integer.parseInt(pkIssuedParam);
			}
		}

		Issued issued = getIssueFromRequest(request, pkIssued);
		if (actionType == ActionType.DELETE) {
			issued.setPkIssued(pkIssued);
		}

		return issued;
	}

	private Issued getIssueFromRequest(HttpServletRequest request, Integer pkIssued) {
		Integer fkBook = Integer.parseInt(request.getParameter("fkBook"));
		Book book = new Book(fkBook);

		Integer fkStudent = Integer.parseInt(request.getParameter("fkStudent"));
		Student student = new Student(fkStudent);

		Issued issued = new Issued();
		issued.setFkStudent(student);
		issued.setFkBook(book);
		issued.setStart(new Date());

		return issued;
	}

}
