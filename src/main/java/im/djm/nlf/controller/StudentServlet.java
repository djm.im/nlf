package im.djm.nlf.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import im.djm.nlf.data.Student;
import im.djm.nlf.exceptions.LibraryException;
import im.djm.nlf.service.StudentService;

public class StudentServlet extends HttpServlet {

	private static final long serialVersionUID = -2312877992048410259L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		ActionType acctionType = ActionType.getFor(action);

		try {
			switch (acctionType) {
			case LIST: {
				break;
			}
			case ADD: {
				Student student = createStudent(request, ActionType.ADD);
				StudentService.getInstance().add(student);
				break;
			}
			case EDIT: {
				Student student = createStudent(request, ActionType.EDIT);
				StudentService.getInstance().edit(student);
				break;
			}
			case DELETE: {
				Student student = createStudent(request, ActionType.DELETE);
				StudentService.getInstance().delete(student);
				break;
			}
			}

			if (acctionType != ActionType.LIST) {
				request.setAttribute("message", "Action successfully completed.");
			}
		} catch (LibraryException ex) {
			request.setAttribute("errors", ex.getErrorMessages());
		}

		List<Student> students = StudentService.getInstance().findAll();
		request.setAttribute("students", students);

		request.getRequestDispatcher("/WEB-INF/pages/student.jsp").forward(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private Student createStudent(HttpServletRequest request, ActionType actionType)
			throws UnsupportedEncodingException {

		request.setCharacterEncoding("UTF-8");
		Student student = createStudentFromRequest(request, actionType);

		return student;
	}

	private Student createStudentFromRequest(HttpServletRequest request, ActionType actionType) {
		Integer pkStudent = getPkStudent(request, actionType);
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		String studentId = request.getParameter("studentId");
		Student student = new Student(name, surname, studentId);

		if (pkStudent != null) {
			student.setPkStudent(pkStudent);
		}

		return student;
	}

	private Integer getPkStudent(HttpServletRequest request, ActionType actionType) {
		Integer pkStudent = null;
		if (actionType == ActionType.EDIT || actionType == ActionType.DELETE) {
			String pkStudentParam = request.getParameter("pkStudent");
			if (pkStudentParam != null) {
				pkStudent = Integer.parseInt(pkStudentParam);
			}
		}

		return pkStudent;
	}

}