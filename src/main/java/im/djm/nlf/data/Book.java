package im.djm.nlf.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Book")
public class Book implements Serializable {

	private static final long serialVersionUID = -7380014242056819515L;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "fkBook")
	private Collection<Issued> issuedCollection;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pkBook")
	private Integer pkBook;

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 13)
	@Column(name = "isbn")
	private String isbn;

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 45)
	@Column(name = "title")
	private String title;

	@Size(max = 100)
	@Column(name = "author")
	private String author;

	@Basic(optional = false)
	@NotNull
	@Column(name = "count")
	private int count;

	public Book() {
	}

	public Book(Integer pkBook) {
		this.pkBook = pkBook;
	}

	public Book(Integer pkBook, String isbn, String title, int count) {
		this.pkBook = pkBook;
		this.isbn = isbn;
		this.title = title;
		this.count = count;
	}

	public Book(String isbn, String title, String author, int count) {
		this.isbn = isbn;
		this.title = title;
		this.author = author;
		this.count = count;
	}

	public Integer getPkBook() {
		return pkBook;
	}

	public void setPkBook(Integer pkBook) {
		this.pkBook = pkBook;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (pkBook != null ? pkBook.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Book)) {
			return false;
		}
		Book other = (Book) object;
		if ((this.pkBook == null && other.pkBook != null)
				|| (this.pkBook != null && !this.pkBook.equals(other.pkBook))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Book[pkBook = " + pkBook + "]";
	}

	public Collection<Issued> getIssuedCollection() {
		return issuedCollection;
	}

	public void setIssuedCollection(Collection<Issued> issuedCollection) {
		this.issuedCollection = issuedCollection;
	}

	public void decrement() {
		this.count--;
	}

	public void increment() {
		this.count++;
	}

}
