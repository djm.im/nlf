package im.djm.nlf.data;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Librarian")
public class Librarian implements Serializable {

	private static final long serialVersionUID = -4413808153841762603L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "pkLibrarian")
	private Integer pkLibrarian;

	@Size(max = 45)
	@Column(name = "username", unique = true)
	private String username;

	@Size(max = 45)
	@Column(name = "password")
	private String password;

	public Librarian() {
	}

	public Librarian(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public Integer getPkLibrarian() {
		return pkLibrarian;
	}

	public void setPkLibrarian(Integer pkLibrarian) {
		this.pkLibrarian = pkLibrarian;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (pkLibrarian != null ? pkLibrarian.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Librarian)) {
			return false;
		}
		Librarian other = (Librarian) object;
		if ((this.pkLibrarian == null && other.pkLibrarian != null)
				|| (this.pkLibrarian != null && !this.pkLibrarian.equals(other.pkLibrarian))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Librarian[pkLibrarian = " + pkLibrarian + "]";
	}

}
