package im.djm.nlf.exceptions;

import java.util.Arrays;
import java.util.List;

public class LibraryException extends Exception {

	private static final long serialVersionUID = 8134047015785904927L;

	private final List<String> errorMessages;

	public LibraryException(List<String> errorMessages) {
		super("Validation of input parameters failed.");
		this.errorMessages = errorMessages;
	}

	public LibraryException(String errorMessage) {
		super("Validation of input parameters failed.");
		this.errorMessages = Arrays.asList(errorMessage);
	}

	public LibraryException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorMessages = Arrays.asList(errorMessage);
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}

}
