package im.djm.nlf.service;

import java.util.List;

import javax.persistence.EntityManager;

import im.djm.nlf.dao.BookDAO;
import im.djm.nlf.dao.EntityManagerWrapper;
import im.djm.nlf.dao.IssuedDAO;
import im.djm.nlf.data.Book;
import im.djm.nlf.data.Issued;
import im.djm.nlf.exceptions.LibraryException;

public class IssuedService {

	private static final IssuedService instance = new IssuedService();

	private IssuedService() {
	}

	public static IssuedService getInstance() {
		return instance;
	}

	public List<Issued> findAll() {
		return IssuedDAO.getInstance().findAll(EntityManagerWrapper.getEntityManager());
	}

	// TODO: to big method -- it should be refactorred
	public void add(Issued issued) throws LibraryException {
		EntityManager em = EntityManagerWrapper.getEntityManager();
		try {
			em.getTransaction().begin();
			Book book = BookDAO.getInstance().findById(em, issued.getFkBook().getPkBook());
			if (book.getCount() > 0) {
				book.decrement();
				BookDAO.getInstance().edit(em, book);

				IssuedDAO.getInstance().add(em, issued);
				em.getTransaction().commit();
			} else {
				em.getTransaction().rollback();
				throw new LibraryException(
						"You cannot the borrow " + book.getTitle() + ". There is no any copy of the book.");
			}
		} finally {
			em.close();
		}
	}

	public void delete(Issued issued) throws LibraryException {
		if (issued.getPkIssued() == null) {
			throw new LibraryException("Unknown issue id.");
		}

		EntityManager em = EntityManagerWrapper.getEntityManager();
		try {
			em.getTransaction().begin();

			Book book = BookDAO.getInstance().findById(em, issued.getFkBook().getPkBook());
			book.increment();

			BookDAO.getInstance().edit(em, book);
			IssuedDAO.getInstance().delete(em, issued);

			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

}
