package im.djm.nlf.service;

import java.util.List;

import javax.persistence.EntityManager;

import im.djm.nlf.dao.BookDAO;
import im.djm.nlf.dao.EntityManagerWrapper;
import im.djm.nlf.data.Book;
import im.djm.nlf.exceptions.LibraryException;

public class BookService {

	private static final BookService instance = new BookService();

	private BookService() {
	}

	public static BookService getInstance() {
		return instance;
	}

	public List<Book> findAll() {
		return BookDAO.getInstance().findAll(EntityManagerWrapper.getEntityManager());
	}

	public void add(Book book) throws LibraryException {
		// validate input parameters (from input form)
		EntityManager em = EntityManagerWrapper.getEntityManager();
		try {
			em.getTransaction().begin();
			BookDAO.getInstance().add(em, book);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

	public void edit(Book book) throws LibraryException {
		// validate input parameters (from input form)

		EntityManager em = EntityManagerWrapper.getEntityManager();
		try {
			em.getTransaction().begin();
			BookDAO.getInstance().edit(em, book);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

	public void delete(Book book) throws LibraryException {
		// validate input parameters (from input form)

		EntityManager em = EntityManagerWrapper.getEntityManager();
		try {
			em.getTransaction().begin();
			BookDAO.getInstance().delete(em, book);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

	// TODO: add validation book
	private void validate(Book book) throws LibraryException {
		//
	}

}
