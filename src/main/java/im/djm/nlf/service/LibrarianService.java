package im.djm.nlf.service;

import javax.persistence.EntityManager;

import im.djm.nlf.dao.EntityManagerWrapper;
import im.djm.nlf.dao.LibrarianDAO;
import im.djm.nlf.data.Librarian;
import im.djm.nlf.exceptions.LibraryException;

public class LibrarianService {

	private static final LibrarianService instance = new LibrarianService();

	private LibrarianService() {
	}

	public static LibrarianService getInstance() {
		return instance;
	}

	public boolean createLibratian(Librarian createNewLibrarian) throws LibraryException {
		EntityManager em = EntityManagerWrapper.getEntityManager();
		try {
			em.getTransaction().begin();
			boolean librarianCreatedSuccessful = LibrarianDAO.getInstance().createLibrarian(em, createNewLibrarian);
			em.getTransaction().commit();

			return librarianCreatedSuccessful;
		} finally {
			em.close();
		}
	}

	public boolean authenticateLibrarian(Librarian librarian) throws LibraryException {
		EntityManager em = EntityManagerWrapper.getEntityManager();
		try {
			em.getTransaction().begin();
			boolean authenticationSuccessful = LibrarianDAO.getInstance().checkCredentials(em, librarian);
			em.getTransaction().commit();

			return authenticationSuccessful;
		} finally {
			em.close();
		}
	}

}
