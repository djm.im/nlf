package im.djm.nlf.dao;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerWrapper {
	// Constants
	private static final String JAVAX_PERSISTENCE_JDBC_URL = "javax.persistence.jdbc.url";
	private static final String JAVAX_PERSISTENCE_JDBC_PASSWORD = "javax.persistence.jdbc.password";
	private static final String JAVAX_PERSISTENCE_JDBC_USER = "javax.persistence.jdbc.user";

	private static EntityManagerFactory emf = null;

	public static EntityManager getEntityManager() {
		try {
			Map<String, String> propertyMap = getPersistencePropertyMap();

			if (emf == null) {
				emf = Persistence.createEntityManagerFactory("LibraryPU", propertyMap);
			}

			return emf.createEntityManager();
		} catch (URISyntaxException ex) {
			// TODO: throw the LibraryException
			ex.printStackTrace();

			// throw new LibraryException("Cannot parse database URI.", ex);
			// TODO: do not return null
			return null;
		}
	}

	private static Map<String, String> getPersistencePropertyMap() throws URISyntaxException {
		// Default values
		// if cannot load values from environment variable these values will be used to
		// connect to database
		Map<String, String> propertyMap = getDefaultpersistencePropertyMap();

		String dbEnvUri = getDatabaseUriFromEnv();
		propertyMap = updateDefaultPropertyMap(propertyMap, dbEnvUri);

		return propertyMap;
	}

	private static Map<String, String> updateDefaultPropertyMap(Map<String, String> propertyMap, String dbEnvUri)
			throws URISyntaxException {

		if (dbEnvUri == null) {
			return propertyMap;
		}

		URI dbUri = new URI(dbEnvUri);

		String username = dbUri.getUserInfo().split(":")[0];
		String password = dbUri.getUserInfo().split(":")[1];

		propertyMap.put(JAVAX_PERSISTENCE_JDBC_USER, username);
		propertyMap.put(JAVAX_PERSISTENCE_JDBC_PASSWORD, password);

		String mysqlDbUrl = "jdbc:mysql://" + dbUri.getHost() + ":" + dbUri.getPort() + dbUri.getPath();
		propertyMap.put(JAVAX_PERSISTENCE_JDBC_URL, mysqlDbUrl);

		return propertyMap;
	}

	private static String getDatabaseUriFromEnv() {
		// Heroku environment uses ClearDB database
		String getenv = System.getenv("JAWSDB_URL");
		if (getenv == null) {
			// Development environment
			getenv = System.getenv("DATABASE_URL");
		}

		return getenv;
	}

	private static Map<String, String> getDefaultpersistencePropertyMap() {
		Map<String, String> propertyMap = new HashMap<>();
		propertyMap.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
		propertyMap.put("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");

		String dbUriString = "jdbc:mysql://localhost:3306/library?characterEncoding=UTF-8";
		propertyMap.put(JAVAX_PERSISTENCE_JDBC_URL, dbUriString);
		propertyMap.put(JAVAX_PERSISTENCE_JDBC_USER, "_USERNAME_");
		propertyMap.put(JAVAX_PERSISTENCE_JDBC_PASSWORD, "_PASSWORD_");

		// Create or update schema
		propertyMap.put("hibernate.hbm2ddl.auto", "update");

		return propertyMap;
	}

}
