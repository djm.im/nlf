package im.djm.nlf.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import im.djm.nlf.data.Book;
import im.djm.nlf.data.Issued;

public class BookDAO {

	private static BookDAO instance = new BookDAO();

	private BookDAO() {
	}

	public static BookDAO getInstance() {
		return instance;
	}

	@SuppressWarnings("unchecked")
	public List<Book> findAll(EntityManager em) {
		Query query = em.createQuery("SELECT book FROM Book book");
		return (List<Book>) query.getResultList();
	}

	public Book findById(EntityManager em, Integer pkBook) {
		return em.find(Book.class, pkBook);
	}

	public Book add(EntityManager em, Book book) {
		em.persist(book);
		return book;
	}

	public Book edit(EntityManager em, Book book) {
		em.merge(book);
		return book;
	}

	public Book delete(EntityManager em, Book book) {
		Book managedBook = em.merge(book);
		em.remove(managedBook);
		return book;
	}

	public long howManyBooksInLibrary(EntityManager em, Issued issued) {
		Query query = em.createQuery("SELECT count(*) FROM Book WHERE pkBook=:pkBook");
		query.setParameter("pkBook", issued.getFkBook().getPkBook());

		return (long) query.getSingleResult();
	}

}
