package im.djm.nlf.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import im.djm.nlf.data.Issued;

public class IssuedDAO {

	private static IssuedDAO instance = new IssuedDAO();

	private IssuedDAO() {
	}

	public static IssuedDAO getInstance() {
		return instance;
	}

	@SuppressWarnings("unchecked")
	public List<Issued> findAll(EntityManager em) {
		Query query = em.createQuery("SELECT issued FROM Issued issued");
		return query.getResultList();
	}

	public void add(EntityManager em, Issued issued) {
		em.persist(issued);
	}

	public void delete(EntityManager em, Issued issued) {
		Query query = em.createQuery("DELETE FROM Issued issued where issued.pkIssued=:pkIssued");
		query.setParameter("pkIssued", issued.getPkIssued());
		query.executeUpdate();
	}

}
