package im.djm.nlf.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AccessFilter implements Filter {

    private static final List<String> NO_AUTH_URLS = Arrays.asList("/index.jsp", "/logout", "/login", "/signup");

    public AccessFilter() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;

        String url = httpRequest.getServletPath();

        if (!NO_AUTH_URLS.contains(url)) {
            HttpSession session = httpRequest.getSession();
			if (session.getAttribute("librarian") == null) {
                String[] errorMessage = new String[]{"You have to be signed in to see students, books and issued books."};
				httpRequest.setAttribute("errors", errorMessage);
                RequestDispatcher requestDispatcher = httpRequest.getRequestDispatcher("index.jsp");
				requestDispatcher.forward(request, response);
            }
        }

        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

}
