<!DOCTYPE html>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>

<body style="padding-top: 80px;">
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand"><span>NLF</span></a>
			</div>

			<div class="collapse navbar-collapse" id="navbar-ex-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="student">Students</a></li>
					<li><a href="book">Books</a></li>
					<li><a href="issued">Issued</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right"></ul>
				<a class="btn btn-default navbar-btn pull-right" href="logout">Sign Out</a>
			</div>
		</div>
	</div>

	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12"></div>
			</div>

			<c:if test="${not empty errors}">
				<div class="row">
					<div class="col-md-12">

						<div class="alert alert-danger" role="alert">
							<c:forEach items="${errors}" var="error">
								<span class="glyphicon glyphicon-exclamation-sign"
									aria-hidden="true"></span>
								<span class="sr-only">Error:</span>
                                    ${error} <br />
							</c:forEach>
						</div>
					</div>
				</div>
			</c:if>

			<c:if test="${not empty message}">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-success" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign"
								aria-hidden="true"></span> <span class="sr-only">Message:</span>
							${message}
						</div>
					</div>
				</div>
			</c:if>

			<div class="row">
				<div class="col-md-12">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th>Surname</th>
								<th>Student ID</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${students}" var="student">
								<tr>
									<td id="td_${student.pkStudent}">${student.pkStudent}</td>
									<td id="td_name_${student.pkStudent}">${student.name}</td>
									<td id="td_surname_${student.pkStudent}">${student.surname}</td>
									<td id="td_studentid_${student.pkStudent}">${student.studentId}</td>
									<td>
										<a class="btn btn-block btn-info" data-toggle="modal"
												data-target="#modal-student-edit"
												onclick="fillFields('${student.pkStudent}', 'edit');">
											Edit..
										</a>
									</td>
									<td>
										<a class="btn btn-block btn-danger"
												data-toggle="modal" data-target="#modal-clan-delete"
												onclick="fillFields('${student.pkStudent}', 'delete');">
											Delete..
										</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<a class="btn btn-success pull-right" data-toggle="modal"
							data-target="#modal-student-add">
						New Student..
					</a>
				</div>
			</div>
		</div>
	</div>

	<!-- modal new student -->
	<div class="modal fade" id="modal-student-add">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">x</button>
					<h4 class="modal-title">Enter student data</h4>
				</div>

				<form class="form-horizontal" role="form" action="student"
					method="POST">
					<div class="modal-body">
						<input type="hidden" name="action" value="add" />

						<div class="form-group">
							<div class="col-sm-2">
								<label for="name" class="control-label">Name</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="name"
									placeholder="Name" name="name">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="surname" class="control-label">Surname</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="surname"
									placeholder="Surname" name="surname">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="studentId" class="control-label">Student Id</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="studentId"
									placeholder="Student Id" name="studentId">
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<a class="btn btn-default" data-dismiss="modal">Cancel</a>
						<button type="submit" class="btn btn-primary">Add</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- modal edit student -->
	<div class="modal fade" id="modal-student-edit">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">x</button>
					<h4 class="modal-title">Edit student's data</h4>
				</div>

				<form class="form-horizontal" role="form" action="student" method="POST">
					<div class="modal-body">
						<input type="hidden" name="action" value="edit" />
						<input type="hidden" id="pkStudent-edit" name="pkStudent" value="" />

						<div class="form-group">
							<div class="col-sm-2">
								<label for="name" class="control-label">Name</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="name-edit" value="" name="name">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="surname" class="control-label">Surname</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="surname-edit" value="" name="surname">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label for="studentId" class="control-label">Student Id</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="studentid-edit" value="" name="studentId">
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<a class="btn btn-default" data-dismiss="modal">Cancel</a>

						<button type="submit" class="btn btn-primary">Edit</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- modal delete -->
	<div class="modal fade" id="modal-clan-delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">x</button>
					<h4 class="modal-title">Delete student</h4>

					<form class="" role="form" action="student" method="POST">
						<input type="hidden" name="action" value="delete" />
						<input type="hidden" id="pkStudent-delete" name="pkStudent" value="" />

						<input type="hidden" id="name-delete" name="name" value="" />
						<input type="hidden" id="surname-delete" name="surname" value="" />
						<input type="hidden" id="studentid-delete" name="studentid" value="" />

						<div class="modal-footer">
							<a class="btn btn-default" data-dismiss="modal">Cancel</a>

							<button type="submit" class="btn btn-primary">Delete</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script>
		function fillFields(pkStudent, action) {
			$("#pkStudent-" + action).val(pkStudent);
			$("#name-" + action).val($("#td_name_" + pkStudent).html());
			$("#surname-" + action).val($("#td_surname_" + pkStudent).html());
			$("#studentid-" + action).val($("#td_studentid_" + pkStudent).html());
		}
	</script>
</body>
</html>
